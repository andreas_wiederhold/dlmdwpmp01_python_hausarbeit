# -*- coding: utf-8 -*-
"""Class with unit tests.

The module contains a class which inherits from Python unittest module to test
functions.

    Typical usage example:
    
    unittest.main(test_class)
"""

import os
import unittest
import logging
import pandas as pd
from package.classes import DatabaseClass
from package.utils import get_files_in_folder
from package import config as gl

logger = logging.getLogger(__name__)

# Import csv files into pandas dataframes
data_path = os.path.join(gl.base_path, 'data')

files = get_files_in_folder(data_path)

df_ideal = pd.read_csv(os.path.join(data_path, files[0]))
df_test = pd.read_csv(os.path.join(data_path, files[1]))
df_train = pd.read_csv(os.path.join(data_path, files[2]))

obj = DatabaseClass(df_train, df_ideal, df_test)

class UnitTests(unittest.TestCase):
    """
    Class which inherits from unittest module.
    """

    def test_file_list(self):
        """
        Test on get_files_in_folder function.
        """
        result = get_files_in_folder(gl.base_path)
        self.assertIsInstance(result, list, 'Rueckgabe sollte eine Liste sein.')

    def test_index_list(self):
        """
        Test on get_index method of DatabaseClass.
        """
        result = obj.get_index()
        self.assertIsInstance(result[0], int, 'Liste sollte Integers enthalten.')

    def test_create_dataframe(self):
        """
        Test on create_deviation_dataframe method of DatabaseClass.
        """
        result = obj.create_deviation_dataframe([0])
        self.assertIsInstance(result, pd.DataFrame, 'Rueckgabe sollte ein Dataframe sein.')
