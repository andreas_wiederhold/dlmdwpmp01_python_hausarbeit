# -*- coding: utf-8 -*-
"""Main module.

    Typical usage example:
    
    run script in Anaconda Prompt with
    python main.py
"""

import os
import unittest
import logging
import pandas as pd
from package import setup_logging
from package import config as gl
from package import utils
from package import classes
from tests import test_class

logger = logging.getLogger(__name__)

def main():
    """
    Main function to start app.
    """
    logger.info('Start App')

    # Import csv files into pandas dataframes
    data_path = os.path.join(gl.base_path, 'data')

    files = utils.get_files_in_folder(data_path)

    df_ideal = pd.read_csv(os.path.join(data_path, files[0]))
    df_test = pd.read_csv(os.path.join(data_path, files[1]))
    df_train = pd.read_csv(os.path.join(data_path, files[2]))

    # Create database files
    classes.DatabaseClass.create_db(df_train, 'database_files/df_train')
    classes.DatabaseClass.create_db(df_ideal, 'database_files/df_ideal')

    # Assign ideal function to training data
    obj = classes.DatabaseClassPlots(df_train, df_ideal, df_test)
    obj_plot = classes.DatabaseClassPlots(df_train, df_ideal, df_test)

    index_lst = obj.get_index()

    # Create plots with ideal and training data
    obj_plot.plot_ideal_and_training(index_lst)

    # Create dataframe with deviation data
    df_dev = obj.create_deviation_dataframe(index_lst)

    # Filter dataframe with regard to outlier assignment
    df_export = df_dev.loc[df_dev['Nummer der Idealen Funktion'] != 'outlier']
    df_outliers = df_dev.loc[df_dev['Nummer der Idealen Funktion'] == 'outlier']

    # Create database files
    classes.DatabaseClass.create_db(df_export, 'database_files/test_ideal')

    obj_plot.plot_ideal_and_training_with_outliers(index_lst, df_export, df_outliers)


if __name__ == "__main__":
    setup_logging.setup_logging()
    unittest.main(test_class)
    main()
