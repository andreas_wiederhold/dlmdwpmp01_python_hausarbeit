# -*- coding: utf-8 -*-
"""File contains constants which may be necessary in different modules.

Everything in this file is to be treated as an immutable object.

    Typical usage example:
    
    config.py import as gl = global
    import config as gl
"""

import os

base_path = os.getcwd()

dt_format = '%d%m%y_%H%M%S'
