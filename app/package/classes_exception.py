# -*- coding: utf-8 -*-
"""Class with user-defined exceptions.

The module contains user-defined exceptions to give back specials messages.

    Typical usage example:

    try:
        ...
    else:
        raise UserDefinedException
    except UserDefinedException:
        print(UserDefinedException().exception_msg)
"""

import logging

logger = logging.getLogger(__name__)

class UserDefinedException(Exception):
    """User Defined Exception Class.

    Attributes:
        Exception (type): Exception.
    """
    def __init__(self, *args, **kwargs):
        """Inits UserDefinedException.

        Args:
            See class attributes.
        """
        super().__init__(self, *args, **kwargs)

        exception_msg = 'Keine idealen Funktionen vorhanden.'
        self.exception_msg = exception_msg
