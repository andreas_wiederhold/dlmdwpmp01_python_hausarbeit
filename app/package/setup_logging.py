# -*- coding: utf-8 -*-
"""Functions for proper logging with the logging module.

Can be extended to console logging or time rotating file handler.

  Typical usage example:

  setup_logging()
"""

import logging

def get_handler_file():
    """
    Return a handler for logging to a machine readable file.
    """
    log_file = "logfiles/logfile.log"
    date_fmt = "%Y-%m-%d %H:%M:%S"
    log_fmt = "[{asctime} {name}] {levelname}: {message}"
    formatter = logging.Formatter(log_fmt, date_fmt, style="{")
    handler = logging.FileHandler(log_file, mode="a")
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)

    return handler

def setup_logging():
    """
    Set up and configure logging functionality.
    """
    # suppress logging from imported modules with argument '__main__'
    root_logger = logging.getLogger('__main__')

    # NOTSET = use levels from handler
    root_logger.setLevel(logging.NOTSET)

    # call handler
    root_logger.addHandler(get_handler_file())
