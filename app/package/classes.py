# -*- coding: utf-8 -*-
"""Classes for handling dataframes and creating database files.

The module contains the class DatabaseClass to create database files and to
handle dataframes and a class DatabaseClassPlots which inherits from
DatabaseClass designed specifically to plot data.

    Typical usage example:

    obj = DatabaseClass(df_train, df_ideal, df_test)
    index_lst = obj.get_index()
"""

from datetime import datetime
import logging
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
import matplotlib.pyplot as plt
from package import config as gl
from package import classes_exception

logger = logging.getLogger(__name__)

# Set font size of matplotlib figures
plt.rcParams.update({'font.size': 14})

class DatabaseClass():
    """Class with main methods to handle dataframes.

    Attributes:
        df_train: Dataframe containing training data.
        df_ideal: Dataframe containing training ideal functions.
        df_test: Dataframe containing test data.
    """
    def __init__(self, df_train, df_ideal, df_test):
        """Inits DatabaseClass.

        Args:
            See class attributes.
        """
        super().__init__()
        self.df_train = df_train
        self.df_ideal = df_ideal
        self.df_test = df_test

    def get_index(self):
        """
        Returns integer list with ideal function indices.
        """
        train_function_names = list(self.df_train)[1:]
        ideal_function_names = list(self.df_ideal)[1:]

        idx_lst = []

        for train_function in train_function_names:
            sum_of_squared_residuals_lst = []
            for ideal_function in ideal_function_names:
                sum_of_squared_residuals_lst.append(((self.df_train[train_function]-\
                                                      self.df_ideal[ideal_function])**2).sum())

            val, idx = min((val, idx) for (idx, val) in enumerate(sum_of_squared_residuals_lst))
            idx_lst.append(idx)

        return idx_lst

    def create_db(df, name):
        """
        Create Database file.

        Args:
            df: Dataframe from csv file.
            name: File name of database.

        Raises:
            ValueError: File already exists.
        """
        engine = create_engine('sqlite:///' + name + '.db', echo = True)
        connection = engine.connect()

        # If file exists, add timestamp to file name
        try:
            df.to_sql(name, connection, index=False, if_exists='fail')
        except ValueError:
            logger.error('File already exists.')

            now_str = datetime.strftime(datetime.now(), gl.dt_format)
            name = name + '_' + now_str

            logger.info('New file name is: %s', name)
            
            engine = create_engine('sqlite:///' + name + '.db', echo = True)
            connection = engine.connect()
            
            df.to_sql(name, connection, index=False, if_exists='fail')

        connection.close()

    def create_deviation_dataframe(self, index_lst):
        """
        Returns dataframe with deviation data.

        Args:
            index_lst: List with indices of ideal functions.
        """
        df = pd.DataFrame()

        # Create list of strings with ideal function names
        index_str_lst = ['y'+str(e+1) for e in index_lst]

        # Choose subset of ideal data
        try:
            if len(index_str_lst):
                df_ideal_subset = self.df_ideal[index_str_lst]
            else:
                raise classes_exception.UserDefinedException
        except classes_exception.UserDefinedException:
            print(classes_exception.UserDefinedException().exception_msg)


        # Iterate over test data
        for _, row in self.df_test.iterrows():
            x_test = row['x']
            y_test = row['y']

            # Find row number of x in ideal data
            row_nr = self.df_ideal[self.df_ideal['x']==x_test].index[0]

            # Create dataframe with all y-values of ideal functions for x_test
            df_ideal_subset_row = df_ideal_subset.iloc[row_nr, :].T.to_frame()

            # Find ideal function which value is nearest to y_test
            ideal_idx = df_ideal_subset_row[row_nr].sub(y_test).abs().idxmin()

            # Find column index of df_train for corresponding ideal function
            df_train_col_idx = index_str_lst.index(ideal_idx) + 1

            # Calculate differences between ideal functions and test/training data
            diff_1 = abs(df_ideal_subset_row.loc[ideal_idx].values[0] - y_test)
            diff_2 = abs(df_ideal_subset_row.loc[ideal_idx].values[0] - \
                         self.df_train.iloc[row_nr, df_train_col_idx])

            # Calculate ratio of deviation of test value and ideal value
            try:
                # dev = y_test/test_df.loc[ideal_idx].values[0]
                dev = diff_1/diff_2
            except ZeroDivisionError:
                logger.error('Division by 0 is not allowed.')

            # If deviation does not fulfill square-2-criterion, mark as outlier
            if dev > np.sqrt(2):
                ideal_idx = 'outlier'

            # Create temporary dataframe
            df_temp = pd.DataFrame({'X (Test Funktion)': x_test,
                                    'Y1 (Test Funktion)': y_test,
                                    'Delta Y (Abweichung)': dev,
                                    'Nummer der Idealen Funktion': ideal_idx},
                                   index=[0])

            # Append temporary one-row-dataframe to deviation dataframe
            df = pd.concat([df, df_temp], ignore_index=True)

        return df


class DatabaseClassPlots(DatabaseClass):
    """Class which inherits from DatabaseClass contains methods to plot data.

    Attributes:
        See attributes of DatabaseClass.
    """
    def __init__(self, df_train, df_ideal, df_test):
        """Inits DatabaseClassPlots.

        Args:
            See class attributes of DatabaseClass.
        """
        super().__init__(df_train, df_ideal, df_test)

    def plot_ideal_and_training(self, index_lst):
        """
        Create plot with matching ideal and training data.

        Args:
            index_lst: List with indices of ideal functions.
        """
        # Create plots with ideal and training data
        plt.figure(1)

        for train_idx, ideal_idx in enumerate(index_lst):
            # Close figure to prevent overwriting of plots with new data
            plt.close(1)

            plt.plot(self.df_train['x'], self.df_train.iloc[:, train_idx+1],
                     marker='o', markersize=5, linestyle = 'None',
                     label='y'+str(train_idx+1))
            plt.plot(self.df_ideal['x'], self.df_ideal.iloc[:, ideal_idx+1], 'k--',
                     label='y'+str(ideal_idx+1))
            plt.xlabel('x')
            plt.ylabel('y')
            plt.legend(loc='lower right')
            plt.grid()
            plt.savefig('figures/picture_' + "{:02d}".format(train_idx) + '.pdf')
        plt.close(1)

    def plot_ideal_and_training_with_outliers(self, index_lst, df_export, df_outliers):
        """
        Create plot with matching ideal and training data.

        Args:
            index_lst: List with indices of ideal functions.
            df_export: Dataframe containing fitting test data.
            df_outliers: Dataframe containing outlier data.
        """
        # Create plot with ideal and training data
        fig = plt.figure()
        ax = plt.subplot(111)

        # Loop over ideal functions
        for train_idx, ideal_idx in enumerate(index_lst):
            ax.plot(self.df_train['x'], self.df_train.iloc[:, train_idx+1],
                    marker='o', markersize=3, linestyle = 'None',
                    label='y_train_'+str(train_idx+1))
            ax.plot(self.df_ideal['x'], self.df_ideal.iloc[:, ideal_idx+1], 'k--')

            # Plot name of ideal function to corresponding graph
            ax.text(self.df_ideal['x'][0],
                    self.df_ideal.iloc[:, ideal_idx+1][0]+2,
                    list(self.df_ideal)[ideal_idx+1])

        # Plot adaptable test data and outliers
        ax.plot(df_export['X (Test Funktion)'], df_export['Y1 (Test Funktion)'],
                'ko', markersize=5, label='non-outliers')
        ax.plot(df_outliers['X (Test Funktion)'], df_outliers['Y1 (Test Funktion)'],
                'mo', markersize=3, label='outliers')

        # Shrink current axis's height by 10% on the bottom
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.2,
                         box.width, box.height * 0.9])

        plt.xlabel('x')
        plt.ylabel('y')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
        ax.grid()
        plt.savefig('figures/picture_all.pdf')
