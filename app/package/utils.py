# -*- coding: utf-8 -*-
"""This is a place for small snippets that can be used throughout the application.
"""

from os import listdir
from os.path import isfile, join
import logging

logger = logging.getLogger(__name__)

def get_files_in_folder(path):
    """
    Get all files which are located in a folder.

    Args:
        path: Full path of folder.

    Returns:
        List of file names.
    """
    return [f for f in listdir(path) if isfile(join(path, f))]
